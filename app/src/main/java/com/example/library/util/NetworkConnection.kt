package com.example.library.util

import android.content.Context
import android.net.ConnectivityManager

object NetworkConnection {
    fun checkInternetConnection(context: Context?): Boolean {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.getNetworkCapabilities(cm.activeNetwork) ?: return false
        return true
    }
}