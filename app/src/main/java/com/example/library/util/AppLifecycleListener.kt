package com.example.library.util

import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner

class AppLifecycleListener(val fetchData: () -> Unit) :
    DefaultLifecycleObserver {

    override fun onStart(owner: LifecycleOwner) { // app moved to foreground
        Log.d("LifecycleOwner", " app moved to foreground")
        fetchData()
    }

    override fun onStop(owner: LifecycleOwner) { // app moved to background
        Log.d("LifecycleOwner", " app moved to background")
    }
}