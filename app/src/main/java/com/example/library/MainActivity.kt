package com.example.library

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.library.databinding.ActivityMainBinding
import com.example.library.model.Book
import com.example.library.util.AppLifecycleListener
import com.example.library.util.DataState
import com.example.library.util.NetworkConnection
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var booksAdapter: BooksAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this, R.layout.activity_main
        ).apply {
            recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)

            booksAdapter = BooksAdapter {
                val intent = Intent(this@MainActivity, BookDetailActivity::class.java).apply {
                    putExtra(EXTRA_TITLE, it.title)
                    putExtra(EXTRA_AUTHOR, it.author)
                    putExtra(EXTRA_GENRE, it.genre)
                    putExtra(EXTRA_CONTENT, it.content)
                }
                startActivity(intent)
            }
        }
        binding.recyclerView.adapter = booksAdapter

        progressObservers()
        fetchData()
        subscribeObservers()

        ProcessLifecycleOwner.get().lifecycle.addObserver(AppLifecycleListener { fetchData() })
    }

    private fun fetchData() {
        if (NetworkConnection.checkInternetConnection(this)) {
            viewModel.setStateEvent(MainStateEvent.GetBookEvents)
        } else {
            Toast.makeText(this, "Loading from cache", Toast.LENGTH_SHORT).show()
            viewModel.setStateEvent(MainStateEvent.NoInternet)
        }
    }

    private fun progressObservers() {
        viewModel.inProgress.observe(this, Observer {
            binding.progressbar.visibility = if (it == true) View.VISIBLE else View.GONE
            binding.recyclerView.visibility = if (it == true) View.INVISIBLE else View.VISIBLE
        })
    }

    private fun subscribeObservers() {
        viewModel.dataState.observe(this, Observer { dataState ->
            when (dataState) {
                is DataState.Success<List<Book>> -> {
                    booksAdapter.submitList(dataState.data.toMutableList())
                }
                is DataState.Error -> {
                    Log.d("error", "${dataState.exception}")
                }
                is DataState.Loading -> {
                    // Handle loading state
                }
            }
        })
    }

    companion object {
        private const val EXTRA_TITLE = "EXTRA_TITLE"
        private const val EXTRA_AUTHOR = "EXTRA_AUTHOR"
        private const val EXTRA_GENRE = "EXTRA_GENRE"
        private const val EXTRA_CONTENT = "EXTRA_CONTENT"
    }
}

