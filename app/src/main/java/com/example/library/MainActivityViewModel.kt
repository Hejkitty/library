package com.example.library

import android.util.Log
import androidx.hilt.Assisted
import androidx.lifecycle.*
import com.example.library.model.Book
import com.example.library.repository.MainRepository
import com.example.library.room.BookDao
import com.example.library.room.CacheMapper
import com.example.library.util.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel
@Inject
constructor(
    private val mainRepository: MainRepository,
    private val bookDao: BookDao,
    private val cacheMapper: CacheMapper,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val _dataState: MutableLiveData<DataState<List<Book>>> = MutableLiveData()
    val dataState: LiveData<DataState<List<Book>>>
        get() = _dataState

    private val _requestInProgress = MutableLiveData<Boolean>(true)
    val inProgress: LiveData<Boolean>
        get() = _requestInProgress

    fun setStateEvent(mainStateEvent: MainStateEvent) {
        viewModelScope.launch {
            when (mainStateEvent) {
                is MainStateEvent.GetBookEvents -> {
                    _requestInProgress.value = true
                    bookDao.delete()

                    try {
                        val call1 = async {
                            mainRepository.getFirstBook()
                        }
                        val call2 = async {
                            mainRepository.getSecondBooks()
                        }
                        val call3 = async {
                            mainRepository.getThirdBooks()
                        }
                        val call4 = async {
                            mainRepository.getForthBooks()
                        }
                        val call5 = async {
                            mainRepository.getFifthBooks()
                        }

                        val result =
                            call1.await() + call2.await() + call3.await() + call4.await() + call5.await()

                        for (book in result) {
                            bookDao.insert(cacheMapper.mapToEntity(book))
                        }

                        _requestInProgress.value = false
                        _dataState.value = DataState.Success(result)
                    } catch (e: Exception) {
                        Log.d("Error", "${e.message}")
                    }
                }
                is MainStateEvent.NoInternet -> {
                    _requestInProgress.value = false
                    _dataState.value = DataState.Success(cacheMapper.mapFromEntityList(bookDao.get()))
                }
            }
        }
    }

}


sealed class MainStateEvent {
    object GetBookEvents : MainStateEvent()
    object NoInternet : MainStateEvent()
}