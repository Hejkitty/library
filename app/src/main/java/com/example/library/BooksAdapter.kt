package com.example.library

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.library.model.Book

class BooksAdapter(private val callback: (book: Book) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val booksList: MutableList<Book> = mutableListOf<Book>()

    fun submitList(bookList: MutableList<Book>) {
        this.booksList.clear()
        this.booksList.addAll(bookList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BookHolder.create(parent) { callback(it) }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is BookHolder) holder.bind(booksList[position])
    }

    override fun getItemCount(): Int = booksList.size
}