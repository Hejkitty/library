package com.example.library

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class BookDetailActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_layout)

        val title = intent.getStringExtra(EXTRA_TITLE)
        val author = intent.getStringExtra(EXTRA_AUTHOR)
        val genre = intent.getStringExtra(EXTRA_GENRE)
        val content = intent.getStringExtra(EXTRA_CONTENT)

        findViewById<TextView>(R.id.title).text = title
        findViewById<TextView>(R.id.author).text = author
        findViewById<TextView>(R.id.genre).text = genre
        findViewById<TextView>(R.id.content).text = content
    }

    companion object {
        private const val EXTRA_TITLE = "EXTRA_TITLE"
        private const val EXTRA_AUTHOR = "EXTRA_AUTHOR"
        private const val EXTRA_GENRE = "EXTRA_GENRE"
        private const val EXTRA_CONTENT = "EXTRA_CONTENT"
    }
}